import React from 'react';
import PropTypes from 'prop-types';
import { Row } from 'reactstrap';

import { CharacterCard } from 'components';

const CharactersList = props => {
  const { characters } = props;

  return (
    <Row>
      {characters.map(character => (
        <CharacterCard character={character} key={character.id} />
      ))}
    </Row>
  );
};

CharactersList.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  characters: PropTypes.array.isRequired
};

export default CharactersList;
