import React from 'react';
import { Container } from 'reactstrap';

import { Home } from 'screens';

const App = () => {
  return (
    <Container fluid>
      <Home />
    </Container>
  );
};

export default App;
