import React from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  CardBody,
  CardImg,
  CardTitle,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Col
} from 'reactstrap';

import './styles.scss';

class CharacterCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false
    };
  }

  toggleModal = () => {
    this.setState(prevState => ({
      showModal: !prevState.showModal
    }));
  };

  getLinkTitle = type => {
    switch (type) {
      case 'comiclink':
        return 'Comics Link';
      case 'wiki':
        return 'Wiki';
      default:
        return 'Details';
    }
  };

  render() {
    const { showModal } = this.state;
    const { character } = this.props;
    const image = `${character.thumbnail.path}.${
      character.thumbnail.extension
    }`;

    return (
      <React.Fragment>
        <Col md="4">
          <Card onClick={() => this.toggleModal()}>
            <CardBody className="text-center">
              <CardTitle>
                <h2>{character.name}</h2>
              </CardTitle>
              <CardImg
                bottom
                src={image}
                alt={character.name}
                className="characterImg"
              />
            </CardBody>
          </Card>
        </Col>

        <Modal isOpen={showModal} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>{character.name}</ModalHeader>
          <ModalBody>
            <h5>Character Description</h5>
            {character.description || 'Description unavailable'}
            <hr />
            <h6>Statistics</h6>
            <p>
              This character appears on {character.comics.available} comics and{' '}
              {character.stories.available} stories.
            </p>
            <hr />
            <p>You can get information about it on this links:</p>
            {character.urls.map(url => (
              <p key={url.type}>
                <a href={url.url} target="_blank" rel="noopener noreferrer">
                  {this.getLinkTitle(url.type)}
                  <i className="fas fa-external-link-alt ml-2" />
                </a>
              </p>
            ))}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleModal}>
              Close
            </Button>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

CharacterCard.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  character: PropTypes.object.isRequired
};

export default CharacterCard;
