module.exports = {
  resolve: {
    alias: {
      path: `${__dirname}/../../`,
      modules: 'path/node_modules',
      root: 'path/src',
      actions: 'path/src/actions',
      assets: 'path/src/assets',
      reducers: 'path/src/reducers',
      components: 'path/src/components',
      screens: 'path/src/screens',
      sagas: 'path/src/sagas'
    },
    extensions: ['.js', '.jsx'],
  }
};