/* eslint-disable require-yield */
import { takeEvery, all, call, put, fork } from 'redux-saga/effects';
import axios from 'axios';

import * as types from 'actions/characters/types';

const apiGetCharacters = ({ limit, offset, nameStartsWith, orderBy }) => {
  const { REACT_APP_API_URL, REACT_APP_API_KEY } = process.env;
  const url = `${REACT_APP_API_URL}characters?apikey=${REACT_APP_API_KEY}`;

  return axios
    .get(url, {
      headers: {
        Accept: 'application/json'
      },
      params: {
        limit: limit || 20,
        offset: offset || 0,
        nameStartsWith: nameStartsWith || undefined,
        orderBy: orderBy || 'name'
      }
    })
    .then(r => r.data)
    .catch(err => {
      throw err;
    });
};

export function* getCharacters(args) {
  try {
    const { query } = args;
    const characters = yield call(apiGetCharacters, query);

    yield put({ type: types.CHARACTERS_GET_SUCCESS, payload: characters });
  } catch (error) {
    yield put({ type: types.CHARACTERS_GET_ERROR, error });
  }
}

export function* getCharactersSaga() {
  yield takeEvery(types.CHARACTERS_GET, getCharacters);
}

export default function* rootSaga() {
  yield all([fork(getCharactersSaga)]);
}
