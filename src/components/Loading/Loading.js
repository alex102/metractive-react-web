import React from 'react';
import { Spinner, Row, Col } from 'reactstrap';

const Loading = () => (
  <Row className="justify-content-center mt-5">
    <Col lg="6" md="8" sm="12">
      <h3 className="text-center">
        Have patience, your characters are coming...
        <Spinner color="primary" className="ml-3" type="grow" />
      </h3>
    </Col>
  </Row>
);

export default Loading;
