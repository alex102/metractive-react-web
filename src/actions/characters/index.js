import * as types from './types';

export const getCharacters = query => {
  return {
    type: types.CHARACTERS_GET,
    query
  };
};

export const clearCharacters = () => {
  return {
    type: types.CHARACTERS_CLEAR
  };
};
