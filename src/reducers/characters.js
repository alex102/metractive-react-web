import * as types from 'actions/characters/types';

const initialState = {
  characters: [],
  total: 0,
  offset: 0,
  count: 0,
  limit: 20,
  loading: false,
  error: ''
};

const charactersReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.CHARACTERS_GET:
      return {
        ...state,
        loading: true,
        error: ''
      };
    case types.CHARACTERS_GET_SUCCESS:
      return {
        ...state,
        characters: state.characters.concat(action.payload.data.results),
        total: action.payload.data.total,
        offset: action.payload.data.offset,
        count: state.count + action.payload.data.count,
        loading: false,
        error: ''
      };
    case types.CHARACTERS_GET_ERROR:
      return {
        ...state,
        characters: [],
        offset: 0,
        total: 0,
        count: 0,
        loading: false,
        error: action.error.message
      };
    case types.CHARACTERS_CLEAR:
      return {
        ...state,
        characters: [],
        offset: 0,
        total: 0,
        count: 0,
        loading: true,
        error: ''
      };
    default:
      return state;
  }
};

export default charactersReducer;
