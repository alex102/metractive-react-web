import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Alert, Jumbotron, FormGroup, Input } from 'reactstrap';
import InfiniteScroll from 'react-infinite-scroll-component';

import { getCharacters, clearCharacters } from 'actions/characters';
import { Loading, CharactersList, ScrollButton } from 'components';

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      orderBy: 'name',
      nameStartsWith: ''
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    const { orderBy, nameStartsWith } = this.state;
    const { getCharacters: get, limit } = this.props;

    get({ offset: 0, limit, orderBy, nameStartsWith });
  };

  fetchMoreData = () => {
    const { orderBy, nameStartsWith } = this.state;
    const { getCharacters: get, offset, limit } = this.props;
    const newOffset = offset + limit;

    get({ offset: newOffset, limit, orderBy, nameStartsWith });
  };

  searchByName = async e => {
    e.persist();
    const { clearCharacters: clearList } = this.props;
    await clearList();
    this.setState({ nameStartsWith: e.target.value }, () => this.fetchData());
  };

  render() {
    const { characters, loading, error, total, count } = this.props;

    return (
      <>
        <Jumbotron>
          <h3 className="display-3">Welcome to Marvel Characters List!</h3>
          <hr />
          <FormGroup>
            <Input
              type="text"
              name="search"
              placeholder="Search by names starting with"
              onChange={this.searchByName}
            />
          </FormGroup>
        </Jumbotron>

        {loading && !error && characters.length === 0 && <Loading />}
        {!loading && error && (
          <Alert color="danger">
            <h4 className="alert-heading">Failure</h4>
            <p>
              Sorry, a failure occurred while fetching your characters. Try
              again later.
            </p>
            <hr />
            <p className="mb-0">Message: {error}</p>
          </Alert>
        )}
        {!error && (
          <>
            <InfiniteScroll
              dataLength={count}
              next={this.fetchMoreData}
              hasMore={!loading && count < total}
              loader={<Loading />}
            >
              <CharactersList characters={characters} />
            </InfiniteScroll>
            <ScrollButton scrollStepInPx={50} delayInMs={16.66} />
          </>
        )}
      </>
    );
  }
}

Home.propTypes = {
  getCharacters: PropTypes.func.isRequired,
  clearCharacters: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  characters: PropTypes.array,
  total: PropTypes.number,
  offset: PropTypes.number,
  count: PropTypes.number,
  limit: PropTypes.number,
  loading: PropTypes.bool,
  error: PropTypes.string
};

Home.defaultProps = {
  characters: [],
  total: 0,
  offset: 0,
  count: 0,
  limit: 20,
  loading: false,
  error: ''
};

const mapStateToProps = store => ({
  characters: store.characters.characters,
  total: store.characters.total,
  offset: store.characters.offset,
  count: store.characters.count,
  limit: store.characters.limit,
  loading: store.characters.loading,
  error: store.characters.error
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getCharacters, clearCharacters }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
