import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from 'reducers';
import sagas from 'sagas';

const sagaMiddleware = createSagaMiddleware();
const middleware = [];

middleware.push(sagaMiddleware);
if (process.env.NODE_ENV === 'development') {
  const { logger } = require('redux-logger');

  middleware.push(logger);
}

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(...middleware))
);

sagaMiddleware.run(sagas);

export default store;
