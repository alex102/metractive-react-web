import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

class ScrollButton extends React.Component {
  constructor() {
    super();

    this.state = {
      intervalId: 0
    };
  }

  scrollStep = () => {
    const { intervalId } = this.state;
    const { scrollStepInPx } = this.props;

    if (window.pageYOffset === 0) {
      clearInterval(intervalId);
    }
    window.scroll(0, window.pageYOffset - scrollStepInPx);
  };

  scrollToTop = () => {
    const { delayInMs } = this.props;
    const intervalId = setInterval(this.scrollStep, delayInMs);

    this.setState({ intervalId });
  };

  render() {
    return (
      <button
        type="button"
        title="Scroll to top"
        className="scroll"
        onClick={() => {
          this.scrollToTop();
        }}
      >
        <i className="fas fa-chevron-up" />
      </button>
    );
  }
}

ScrollButton.propTypes = {
  delayInMs: PropTypes.number,
  scrollStepInPx: PropTypes.number
};

ScrollButton.defaultProps = {
  delayInMs: 16.66,
  scrollStepInPx: 50
};

export default ScrollButton;
